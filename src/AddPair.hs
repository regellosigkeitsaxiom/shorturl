module AddPair where

import System.Random
import Config
import System.IO
import System.Environment

addPair = do
    args <- getArgs
    case parseArgs args of
        Nothing -> putStrLn "Expection config file and at least one URL to add"
        Just (location, urls) -> do
            oldPairs <- getPairs location
            pairs <- sequence $ map ( newPair oldPairs ) urls
            h <- openFile location AppendMode
            sequence_ $ map ( hPrint h ) pairs
            hClose h

{-
    Length of short urls to be generated
    It does not affect handling existing entries
-}
shortLength = 4

{- 
    Generates new pair
    Ensures that no collisions happens
    Will loop if namespace is exhausted
-}
newPair :: [ Pair ] -> String -> IO Pair
newPair ps url = do
    g <- newStdGen
    let rnd = mkRandomHex g shortLength
    if elem rnd ( map short ps )
    then do
        putStrLn $ "Collision: " ++ rnd ++ ", trying again"
        newPair ps url
    else do
        let p = Pair rnd url
        putStrLn $ "Added new short URL: " ++ ( show p )
        return p

{-
    Parses list of arguments
    Returns Nothing if its length is less than 2
-}
parseArgs :: [ String ] -> Maybe ( String, [ String ] )
parseArgs args | length args < 2 = Nothing
               | otherwise = Just ( head args, tail args )

{-
    Makes new sequence for short url
    Takes StdGen and length of sequence to be generated
    It is kinda ugly, could be more beautiful (and less clear)
-}
mkRandomHex :: StdGen -> Int -> String
mkRandomHex g len = take len $ map toHex rs
    where rs = randomRs (0,15) g
          toHex :: Int -> Char
          toHex x | x < 10 = head $ show x
                  | x == 10 = 'a'
                  | x == 11 = 'b'
                  | x == 12 = 'c'
                  | x == 13 = 'd'
                  | x == 14 = 'e'
                  | x == 15 = 'f'
                  | otherwise = '0'
