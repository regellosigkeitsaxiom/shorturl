{-# LANGUAGE OverloadedStrings, ScopedTypeVariables #-}

module Main where

import Happstack.Lite
import Text.Blaze.Html5 (Html, (!), a, form, input, p, toHtml, label)
import qualified Text.Blaze.Html5 as H
import Config
import System.Environment
import Control.Monad ( liftM )

{-
    Want sane defaults
-}
main :: IO ()
main = do
    args <- getArgs
    case parseArgs args of
        Nothing -> putStrLn "Expected config file" >> return ()
        Just location -> do
            putStrLn "Starting shorturl service"
            putStrLn $ "Config location: " ++ location
            conf <- readConf location
            serve ( Just defaultServerConfig { port = cfgPort conf } )
                  ( myApp $ pairs conf )

{-
    Main cycle
-}
myApp :: [ Pair ] -> ServerPart Response
myApp pairs = msum $ ( map mkDir pairs ) ++ err404

err404 :: [ ServerPart Response ]
err404 = return $ path asd
    where
    asd :: String -> ServerPart Response
    asd x = notFound . toResponse . H.p . toHtml $ "Ссылки nanolab.link/" ++ x ++ " не существует"

{-
    Make redirection servlet from pair
-}
mkDir :: Pair -> ServerPart Response
mkDir p = dir ( short p ) $ seeOther ( full p ) ( toResponse () )

{-
    If one arg, then OK, else not
-}
parseArgs :: [ String ] -> Maybe String
parseArgs args | length args /= 1 = Nothing
               | otherwise = Just $ head args
