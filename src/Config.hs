{-# LANGUAGE OverloadedStrings #-}
module Config where

import Data.Yaml.Config ( loadYamlSettings, ignoreEnv )
import Data.Aeson
import System.IO ( readFile )
import Data.List ( isPrefixOf, isInfixOf )
import Data.List.Split ( splitOn )

data Pair = Pair
    { short :: String
    , full :: String
    }
instance Show Pair where
    show p = short p ++ " < " ++ full p
instance ToJSON Pair where
    toJSON x = object
        [ "short" .= short x
        , "full" .= full x
        ]
instance FromJSON Pair where
    parseJSON ( Object o ) = Pair <$>
        o .: "short" <*>
        o .: "full"

data Conf = Conf
    { cfgPort :: Int
    , newLength :: Int
    , pairs :: [ Pair ]
    }
    deriving Show
instance ToJSON Conf where
    toJSON x = object
        [ "port" .= cfgPort x
        , "new-URL-length" .= newLength x
        , "pairs" .= pairs x
        ]
instance FromJSON Conf where
    parseJSON ( Object o ) = Conf <$>
        o .: "port" <*>
        o .: "new-URL-length" <*>
        o .: "pairs"

readConf :: FilePath -> IO Conf
readConf f = loadYamlSettings [f] [] ignoreEnv
{-
    Will retrieve and parse pairs from file
    Ignores broken entries
-}
getPairs :: FilePath -> IO [ Pair ]
getPairs location = readFile location >>= return . parse

{-
    "Parser" of config of pairs
-}
parse :: String -> [ Pair ]
parse s = onlyJusts [] $ map purse ( lines s )
    where
    purse x | isInfixOf " < " x = Just $ foo $ splitOn " < " x
            | otherwise = Nothing
    foo x = Pair ( x !! 0 ) ( x !! 1 )
    onlyJusts :: [ a ] -> [ Maybe a ] -> [ a ]
    onlyJusts xs [] = xs
    onlyJusts xs (m:ms) = case m of
        Nothing -> onlyJusts xs ms
        Just j  -> onlyJusts (j:xs) ms
